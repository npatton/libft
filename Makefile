# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: npatton <marvin@42.fr>                     +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/04/17 08:12:07 by npatton           #+#    #+#              #
#    Updated: 2018/07/22 12:23:23 by npatton          ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

NAME	=	libft.a

SRC		=	src/*/*.c

OBJ		=	obj/*.o

INC		=	include

CFLAGS	=	-Wall -Werror -Wextra

CC		=	gcc

all: 		$(NAME)

$(NAME):
			mkdir obj
			$(CC) $(CFLAGS) -c $(SRC) -I $(INC)
			mv *.o obj/
			ar -rc $(NAME) $(OBJ)
			ranlib $(NAME)

clean:
			rm -rf obj

fclean:		clean
			rm -rf $(NAME)

re:			fclean all

c:			clean

f:			fclean

r:			re

.PHONY: 	all clean fclean re c f r
